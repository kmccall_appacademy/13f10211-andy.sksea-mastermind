class Code
  attr_reader :pegs
  
  PEGS = { BLUE: "b",
            RED: "r",
            YELLOW: "y",
            GREEN: "g",
            ORANGE: "o",
            PURPLE: "p"
          }
  
  def initialize(code_arr)
    @pegs = []
    if !code_arr.empty? && code_arr.all? { |c| PEGS.values.include?(c) }
      @pegs = code_arr
    end
  end
  
  def [](i)
    @pegs[i]
  end
  
  def ==(code)
    if code.class == Code
      return @pegs == code.pegs
    end
    false
  end
  
  def exact_matches(code)
    exact = 0
    code.pegs.each.with_index { |c, i| exact += 1 if c == @pegs[i] }
    exact
  end
  
  def near_matches(code)
    near = 0
    code.pegs.uniq.each { |c| near += [@pegs.count(c), code.pegs.count(c)].min }
    code.pegs.each.with_index { |c, i| near -= 1 if c == @pegs[i] }
    near
  end
  
  def self.random
    rn = Random.new
    code = self.new([])
    4.times { code.pegs << PEGS.values[rn.rand(0...PEGS.size)] }
    code
  end
  
  def self.parse(code_str)
    code_str = code_str.downcase
    raise ArgumentError, "invalid colors" if !code_str.chars.all? { |c| PEGS.values.include?(c) }
    code = self.new([])
    code_str.chars.each { |c| code.pegs << c if PEGS.values.include?(c) }  
    code
  end
  
end

class Game
  attr_reader :secret_code, :turns_remaining
  
  def initialize(code=nil)
    @secret_code = code.nil? ? Code.random : code
    @turns_remaining = 10
  end
  
  def get_guess
    @turns_remaining -= 1
    print "Guess: "
    $stdout.flush
    Code.parse(gets.chomp)
  end
  
  def display_matches(code)
    puts "exact matches"
    puts @secret_code.exact_matches(code)
    puts "near matches"
    puts @secret_code.near_matches(code)
    puts ""
  end
  
  def display_remaining_turns
    puts "Turns remaining: #{@turns_remaining}"
  end
  
  def code_cracked?(guess)
    @secret_code.exact_matches(guess) == 4
  end
  
end


