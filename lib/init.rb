require 'pry'
require_relative 'mastermind'

g = Game.new
while g.turns_remaining > 0
  guess = g.get_guess
  g.display_remaining_turns
  g.display_matches(guess)
  if g.code_cracked?(guess)
    puts "You've cracked the code!"
    break
  end
end
